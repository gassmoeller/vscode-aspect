import numpy as np 
import json

with open('resources/parameters.json') as json_file:
    data = json.load(json_file)
# ===================================================
def writeKeyWords(keywords, outfile, depth,filepath='parameters'):
    keywords=np.unique(keywords)
    fpout=open('%s/%s.%d'%(filepath,outfile,depth),'w')
    for w in keywords:
        fpout.write('%s|'%(w))
    fpout.close()

def getAllPossibleValues_dict(obj_dict,parent_key):
    values=''
    key_pattern_des='pattern_description'
    if(key_pattern_des in obj_dict.keys()):
        patterns=obj_dict[key_pattern_des]
        
        if('[Selection ' in patterns):
            # print(patterns)
            patterns = patterns.split('[Selection ')[1].split(' ]')[0]
            values=patterns
            # print(values)
    return values
def dict_depth(d):
    if isinstance(d, dict):
        return 1 + (max(map(dict_depth, d.values())) if d else 0)
    return 0
def hasSubObj(obj_dict):
    for key in obj_dict.keys():
        obj1=obj_dict[key]
        if(type(obj1)==type(obj_dict)):
            return True
    return False
def splitDict2Layers(data):
    max_depth=dict_depth(data)
    data_copy=data.copy()
    objs=[]
    for i in range(0,max_depth):
        data_new={}
        layer1={}
        for key in data.keys():
            obj=data[key]
            layer1[key]=data[key].copy()
            if(hasSubObj(obj)):
                for key2 in obj.keys():
                    data_new[key2]=obj[key2] # gives all the next layers object to a new dict and then refresh `data`
                layer1[key]={'tmp':{}} #delete contents if it has subdict, only need its key name as the "subsection" name
        data=data_new.copy()
        objs.append(layer1) # add object of each layer to a list
    return objs
# loop to write information to files
objs_layers=splitDict2Layers(data)
for i in range(len(objs_layers)-1):
    objs=objs_layers[i]
    par_names,par_values,section_names=[],[],[]
    for key in objs.keys():
        obj=objs[key]
        if(hasSubObj(obj)):
            section_names.append(key.replace('_20',' ')) #
        else:
            par_names.append(key.replace('_20',' ')) #.replace('_20',' ')
            par_values.append(getAllPossibleValues_dict(obj,key))

    writeKeyWords(par_names,'par_names', i)
    writeKeyWords(section_names,'section_names', i)
    writeKeyWords(par_values,'par_values', i)
