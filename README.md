[![](https://img.shields.io/badge/Bug-report-green)](https://gitlab.com/vscode_extension/vscode-aspect/-/issues)

# Visual Studio extension for [ASPECT](https://aspect.geodynamics.org) parameter script

Syntax highlight and parameters/values intellisense.

## Support Further Development
[![PayPal Donations](https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif)](https://www.paypal.me/scibyte/15)


## Features

* Color syntaxing for ASPECT parameter file (.prm).
* Intellisense of ASPECT parameters and values.
### Syntax highlight

* Parameters after `set` keyword
* Values after `=` keyword
* Models after `subsection` keyword

## Screenshot

### Enhanced light theme

![](https://raw.githubusercontent.com/zguoch/PubPic/master/ASPECT/theme_light.png)

### Enhanced dark theme

![](https://raw.githubusercontent.com/zguoch/PubPic/master/ASPECT/theme_dark.png)

### Code auto completion

#### Global parameters and values

![](https://raw.githubusercontent.com/zguoch/PubPic/master/ASPECT/global_par.gif)

#### Subsection (modules)

![](https://raw.githubusercontent.com/zguoch/PubPic/master/ASPECT/subsection.gif)

#### Parameters and values in subsection

![](https://raw.githubusercontent.com/zguoch/PubPic/master/ASPECT/subsection_set.gif)
### Key binding of code auto completion

The above completion is triggered by typing, e.g. type <kbd>=</kbd> or <kbd>Space</kbd>. In addition, users can also force trigger it by shortcut of <kbd>⌘ Command</kbd>+<kbd>D</kbd> in macOS or <kbd>Ctrl</kbd>+<kbd>D</kbd> in Windows/Linux systems.

### Mouse hover prompt

**Under developing ...**

The mouse hover prompt method will be activated only in gmt script lines, which will popup side window to display description of the current module or argument.

## What's New

### Add version selection for parameters, the shortcuts is `cmd+shift+v` for Mac OS, and `ctrl+shift+v` for MS Windows. You just need to enter the shortcuts in the opend `.prm` window, there is a option list popup on the top, and then just select a version you want. 

![alt text](image-1.png)

### Add user configuration for path of parameters.

Someone will ask: the package only have three options(see above), how can I add another version, e.g., `v2.4.0`. There are two ways you can solve it. 

* (1) copy a folder (named e.g., `v2.4.0`) containing `parameters.json` which you like to add to the ASPECT extension to the **Parameters path**. If you don't know where is the parameters path, please see the snapshot above, when you type the shortcuts, the path will display on the lower right.
* (2) Make a path with subfolders of each version containing `parameters.json` file, and then set this path to the user configure option. The folder organization are shown in section [New organization for Parameter files]().

![alt text](image-2.png)
  
### New organization for Parameter files

![alt text](image.png)
