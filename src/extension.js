const vscode = require('vscode');
const  fs = require('fs');
const path = require('path');
const env_lang = vscode.env.language;

exports.activate = async function(context) {
	
	// --------------
	// 初始化parameter path
	let disposable = vscode.commands.registerCommand('extension.selectAspectVersion', async () => {

		// -----------
		const par_path = getPath_parameters();
		if (!fs.existsSync(par_path))
		{
			vscode.window.showErrorMessage('Parameter path not found:'+par_path);
			return ;
		}
		const folderNames = getFolderNames(par_path);
		if(folderNames.length == 0)
		{
			vscode.window.showErrorMessage(`There is no version-subfoder containing 'parameters.json' in the folder.`);
			return;
		}
		
        var options = folderNames;
		// vscode.window.showInformationMessage(`Available versions: ` + folderNames.length);
        const selection = await vscode.window.showQuickPick(options, {
            placeHolder: 'Select ASPECT version'
        });
		
        if (selection) {
			vscode.window.setStatusBarMessage(`ASPECT version set to ${selection}`);
            const config = vscode.workspace.getConfiguration('aspect');
            await config.update('version', selection, vscode.ConfigurationTarget.Global);
            // vscode.window.showInformationMessage(`ASPECT version set to ${selection}`);
        }
    });
	context.subscriptions.push(disposable);
	
	if(env_lang=='zh-cn')
	{
		vscode.window.setStatusBarMessage('ASPECT插件已被激活!');
	}else
	{
		vscode.window.setStatusBarMessage('ASPECT extension has been activated !');
	}
	// 检查licence
	// if(!await envaluateLicence())return;

	// require('./helloword')(context); // helloworld
	// require('./jump-to-definition')(context); // 跳转到定义
	// require('./hover')(context); // 悬停提示
	require('./completion')(context); // 自动补全
	// require('./colorpicker')(context); // 取色器
};
function getPath_parameters()
{
	// 1. default parameters path
	var  extensionPath = vscode.extensions.getExtension ("Zhikui.vscode-aspect").extensionPath;
	var par_path = "";
	if(extensionPath.indexOf('/')==0)
	{
		par_path = extensionPath+"/resources/parameters";
	}else
	{
		par_path = extensionPath+"\\resources\\parameters";
	}
	// 2. user configured parameters path
	const path_parameters_configuration = vscode.workspace.getConfiguration().get("aspect.parameter.path");
	//首先判断配置目录是否存在，如果存在则使用配置目录，否则使用默认目录
	if(path_parameters_configuration)
	{
		if(!fs.existsSync(path_parameters_configuration))
			{
				vscode.window.showWarningMessage(`'aspect.parameter.path = ${path_parameters_configuration}' is not correctly configured, the default parameter file path will be used.`);
			}
			else
			{
				const folderNames = getFolderNames(path_parameters_configuration);
				if(folderNames.length == 0)
				{
					vscode.window.showWarningMessage(`'aspect.parameter.path = ${path_parameters_configuration}', but there is no subfoder containing 'parameters.json' in the path, the default parameter file path will be used.`);
				}else{
					par_path = path_parameters_configuration;
				}
			}
	}
	
	// vscode.window.showInformationMessage(`Parameters path: ` + par_path);
	return par_path;
}
function getFolderNames(dirPath) {
    const folderNames = fs.readdirSync(dirPath).filter(file => {
        return fs.statSync(path.join(dirPath, file)).isDirectory();
    });
    return folderNames;
}
/**
 * 插件被释放时触发
 */
exports.deactivate = function() {
	vscode.window.setStatusBarMessage('ASPECT has been deactivated !');
};