var  vscode = require('vscode');
var  path = require('path');
var  fs = require('fs');

function provideHover(document, position, token) {
    var  fileName    = document.fileName;
    var  line        = document.lineAt(position);
    var  word        = document.getText(document.getWordRangeAtPosition(position));
    var  extensionPath = vscode.extensions.getExtension ("Zhikui.vscode-gmt-pro").extensionPath;
    var  gmt_file = path.basename(fileName);
    var  lineText = line.text.substring(0, position.character).replace(/[\t\n]/g,"");
    if(!(new RegExp("gmt").test(lineText)))
    {
        return;
    }
    
    var env_lang = 'en';
    if(vscode.env.language=='zh-cn')
    {
        env_lang = 'zh-cn';
    }
    return new vscode.Hover('');
    // return new vscode.Hover("GMT悬停提示功能正在开发中，敬请期待...");
    var destPath = extensionPath+"/resources/"+env_lang+"/"+gmt_file+".json";
    if (fs.existsSync(destPath)) {
        var  content = require(destPath);
        if(content[word]=='undefined')
        {
            return new vscode.Hover('');
        }else
        {
            // console.log('hover已生效');
            // hover内容支持markdown语法
            return new vscode.Hover(content[word]);
        }
    }else
    {
        return new vscode.Hover('');
    }
}

module.exports = function(context) {
    // 注册鼠标悬停提示
    context.subscriptions.push(vscode.languages.registerHoverProvider('gmt', {
        provideHover
    }));
};