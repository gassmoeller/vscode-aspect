# 打包

`vsce package`

# 发布

`vsce publish 1.0.0`

在使用publish命令之前需要先用vsce登录，`vsce login zhikui`，然后输入access token，这个是在[微软的Azure账户](https://dev.azure.com/zhikuiguo/_usersSettings/tokens)里面申请的，这个access token只有第一次生成的时候才能拷贝，以后无法在看到，如果没记住就只能在重新生成新的了。如下图所示：

![](images/AzureAccount_snapshot.png)
![](images/AzureAccount_snapshot2.png)